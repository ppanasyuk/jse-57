package ru.t1.panasyuk.tm.component;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.api.IPropertyService;
import ru.t1.panasyuk.tm.api.IReceiverService;
import ru.t1.panasyuk.tm.listener.EntityListener;
import ru.t1.panasyuk.tm.service.PropertyService;
import ru.t1.panasyuk.tm.service.ReceiverService;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private IReceiverService receiverService;

    @NotNull
    @Autowired
    private EntityListener entityListener;

    public void init() {
        receiverService.receive(entityListener);
    }

}