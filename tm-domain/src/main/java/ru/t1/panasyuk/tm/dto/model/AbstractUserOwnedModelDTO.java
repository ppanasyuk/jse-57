package ru.t1.panasyuk.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.constant.DBConst;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractUserOwnedModelDTO extends AbstractModelDTO {

    private static final long serialVersionUID = 1;

    @Nullable
    @Column(name = DBConst.COLUMN_USER_ID, length = 36)
    private String userId;

}