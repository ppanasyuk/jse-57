package ru.t1.panasyuk.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.panasyuk.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.panasyuk.tm.constant.FieldConst;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
@Scope("prototype")
public final class TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDTO> implements ITaskDtoRepository {

    @Override
    protected @NotNull Class<TaskDTO> getEntityClass() {
        return TaskDTO.class;
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.%s = :%s AND m.%s = :%s ORDER BY m.%s",
                getEntityClass().getSimpleName(),
                FieldConst.FIELD_USER_ID,
                FieldConst.FIELD_USER_ID,
                FieldConst.FIELD_PROJECT_ID,
                FieldConst.FIELD_PROJECT_ID,
                FieldConst.FIELD_CREATED
        );
        return entityManager
                .createQuery(jpql, getEntityClass())
                .setParameter(FieldConst.FIELD_USER_ID, userId)
                .setParameter(FieldConst.FIELD_PROJECT_ID, projectId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    public void removeAllByProjectId(@NotNull String userId, @Nullable String projectId) {
        @NotNull final String jpql = String.format(
                "DELETE FROM %s m WHERE m.%s = :%s AND m.%s = :%s",
                getEntityClass().getSimpleName(),
                FieldConst.FIELD_USER_ID,
                FieldConst.FIELD_USER_ID,
                FieldConst.FIELD_PROJECT_ID,
                FieldConst.FIELD_PROJECT_ID
        );
        entityManager
                .createQuery(jpql)
                .setParameter(FieldConst.FIELD_USER_ID, userId)
                .setParameter(FieldConst.FIELD_PROJECT_ID, projectId)
                .executeUpdate();
    }

}