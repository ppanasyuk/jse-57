package ru.t1.panasyuk.tm.api.repository.dto;

import ru.t1.panasyuk.tm.dto.model.SessionDTO;

public interface ISessionDtoRepository extends IUserOwnedDtoRepository<SessionDTO> {
}