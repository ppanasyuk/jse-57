package ru.t1.panasyuk.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.dto.model.UserDTO;

import java.util.Collection;
import java.util.List;

public interface IUserDtoService extends IDtoService<UserDTO> {

    @NotNull
    UserDTO add(@NotNull UserDTO user);

    void clear();

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password);

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password, @Nullable String email);

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password, @Nullable String email, @Nullable Role role);

    boolean existsById(@Nullable String id);

    List<UserDTO> findAll();

    UserDTO findByLogin(@Nullable String login);

    UserDTO findByEmail(@Nullable String email);

    UserDTO findOneById(@Nullable String id);

    UserDTO findOneByIndex(@Nullable Integer index);

    int getSize();

    Boolean isLoginExist(@Nullable String login);

    Boolean isEmailExist(@Nullable String email);

    UserDTO removeById(@Nullable String id);

    void lockUserByLogin(@Nullable String login);

    UserDTO remove(@Nullable UserDTO user);

    UserDTO removeByIndex(@Nullable Integer index);

    @NotNull
    UserDTO removeByLogin(@Nullable String login);

    @NotNull
    UserDTO removeByEmail(@Nullable String email);

    Collection<UserDTO> set(@NotNull Collection<UserDTO> users);

    @NotNull
    UserDTO setPassword(@Nullable String id, @Nullable String password);

    void unlockUserByLogin(@Nullable String login);

    void update(@NotNull UserDTO user);

    @NotNull
    UserDTO updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

}