package ru.t1.panasyuk.tm.service;

import io.qameta.allure.junit4.DisplayName;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.panasyuk.tm.api.service.ITokenService;
import ru.t1.panasyuk.tm.marker.UnitCategory;

@Category(UnitCategory.class)
@DisplayName("Тестирование сервиса токена")
public class TokenServiceTest {

    @Test
    @DisplayName("Получение и установка токена")
    public void getAndSetToken() {
        @NotNull final String testToken = "TEST_TOKEN";
        @NotNull final ITokenService tokenService = new TokenService();
        tokenService.setToken(testToken);
        Assert.assertEquals(testToken, tokenService.getToken());
    }

}